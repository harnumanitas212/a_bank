import 'package:a_bank/colorPick.dart';
import 'package:flutter/material.dart';

class AllAppBar extends AppBar {
  AllAppBar()
      : super(
            elevation: 0.25,
            backgroundColor: Colors.white,
            flexibleSpace: _buildGojekAppBar());

  static Widget _buildGojekAppBar() {
    return new Center(
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Container(
              padding: EdgeInsets.all(6.0),
              decoration: new BoxDecoration(
                  borderRadius:
                      new BorderRadius.all(new Radius.circular(100.0)),
                  color: Colors.transparent),
              alignment: Alignment.centerRight,
              child: Row(
                children: [
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "a_bank",
                    style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: Colors.deepPurple,
                        backgroundColor: Colors.transparent),
                  )
                ],
              ))
        ],
      ),
    );
  }
}
