import 'package:a_bank/appBar.dart';
import 'package:flutter/material.dart';
import 'package:a_bank/models/contact.dart';
import 'package:flutter/cupertino.dart';
import 'package:a_bank/ui/viewDaftarProduk.dart';
import 'package:a_bank/colorPick.dart';

class EntryForm extends StatefulWidget {
  // final Contact contact;

  // EntryForm();

  @override
  EntryFormState createState() => EntryFormState();
}

//class controller
class EntryFormState extends State<EntryForm> {
  // Contact contact;

  // EntryFormState(this.contact);

  // TextEditingController nameController = TextEditingController();
  // TextEditingController usernameController = TextEditingController();
  // TextEditingController deskripsiController = TextEditingController();
  // TextEditingController fotoController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //kondisi
    // if (contact != null) {
    //   nameController.text = contact.name;
    //   usernameController.text = contact.username;
    //   deskripsiController.text = contact.deskripsi;
    //   fotoController.text = contact.foto;
    // }
    //rubah
    return SafeArea(
        child: Scaffold(
            appBar: AllAppBar(),
            body: Padding(
              padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
              child: ListView(
                children: <Widget>[
                  // username
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      // controller: usernameController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Nama lengkap ',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),
                  // nama
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      // controller: nameController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Tempat tanggal lahir',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),

                  // deskripsi
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      // controller: deskripsiController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'NIK',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),

                  // foto
                  Padding(
                    padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                    child: TextField(
                      // controller: fotoController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          labelText: 'Agama',
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.black87)),
                          border: UnderlineInputBorder()),
                      onChanged: (value) {
                        //
                      },
                    ),
                  ),
                  new InkWell(
                    onTap: () {
                      // // Navigator.push(
                      // //     context, MaterialPageRoute(builder: (context) => LauncherPage()));
                      // if (contact == null) {
                      //   // tambah data
                      //   contact = Contact(
                      //       nameController.text,
                      //       usernameController.text,
                      //       deskripsiController.text,
                      //       fotoController.text);
                      // } else {
                      //   // ubah data
                      //   contact.name = nameController.text;
                      //   contact.username = usernameController.text;
                      //   contact.deskripsi = deskripsiController.text;
                      //   contact.foto = fotoController.text;
                      // }
                      // kembali ke layar sebelumnya dengan membawa objek contact
                      // navigateToEntryForm(context);
                    },
                    child: Container(
                      width: 200,
                      padding: EdgeInsets.symmetric(vertical: 15),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey.shade200,
                                offset: Offset(2, 4),
                                blurRadius: 5,
                                spreadRadius: 2)
                          ],
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                              colors: [Warna.orenMuda, Warna.merahKeterangan])),
                      child: Text(
                        'Buat Rekening',
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                    ),
                  )
                ],
              ),
            )));
  }
}

Future<Contact> navigateToEntryForm(
    BuildContext context, Contact contact) async {
  var result = await Navigator.push(context,
      MaterialPageRoute(builder: (BuildContext context) {
    return ViewDaftarProduk();
  }));
  return result;
}
