import 'package:flutter/material.dart';
import 'package:a_bank/appBar.dart';
import 'package:a_bank/colorPick.dart';

class ViewHome extends StatefulWidget {
  ViewHome({Key key}) : super(key: key);

  @override
  _ViewHomeState createState() => _ViewHomeState();
}

class _ViewHomeState extends State<ViewHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AllAppBar(),
      body: _grid(),
    );
  }

  Widget _grid() {
    return new Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: GridView.count(
        childAspectRatio: 8.0 / 9.0,
        shrinkWrap: true,
        mainAxisSpacing: 20,
        crossAxisCount: 2,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://image.flaticon.com/icons/png/512/26/26575.png",
                      cacheHeight: 115,
                      cacheWidth: 115,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "pembayaran",
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://image.flaticon.com/icons/png/512/3069/3069455.png",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Transfer",
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://image.flaticon.com/icons/png/512/271/271035.png",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Credit Card",
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://image.flaticon.com/icons/png/512/3596/3596091.png",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Rekeningku",
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://image.flaticon.com/icons/png/512/2746/2746095.png",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Investasi",
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://img-premium.flaticon.com/png/512/1697/1697117.png?token=exp=1623074841~hmac=c7ea0ed05a35bc07b92d9ed538703eb7",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Pembayaran",
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              )),
            ),
          ),
        ],
      ),
    );
  }

  Widget _produk(String nama, String harga, String url) {
    return new Container(
        color: Warna.grey,
        child: Card(
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: Image.network(
                  url,
                  // "https://asset-a.grid.id/crop/0x0:0x0/360x240/photo/2020/04/09/663219154.png",
                  fit: BoxFit.cover,
                  width: 100,
                  height: 100,
                ),
              ),
              Container(
                padding: EdgeInsets.all(5),
                height: 100,
                margin: EdgeInsets.only(left: 8),
                child: Column(
                  children: [
                    Text(
                      nama,
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black87,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(20),
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.ac_unit_rounded,
                          color: Colors.lightBlueAccent,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "a_bank",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Warna.biruMuda,
                              backgroundColor: Colors.transparent),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                alignment: Alignment.bottomRight,
                padding: EdgeInsets.only(left: 20, bottom: 10),
                height: 100,
                child: Text(
                  harga,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ));
  }
}
