import 'package:a_bank/appBar.dart';
import 'package:a_bank/colorPick.dart';
import 'package:flutter/material.dart';
import 'package:a_bank/viewHome.dart';

class BerandaPage extends StatefulWidget {
  BerandaPage({Key key}) : super(key: key);

  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(appBar: AllAppBar(), body: _grid()),
    );
  }

  Widget _viewList() {
    return ListView(
      children: [
        _produk("Nara 3-Seat Sofa", "Rp. 12000000",
            "https://cdn.shopify.com/s/files/1/2350/5189/products/Nara_3_seat_540x.jpg?v=1580272503"),
        _produk("Newbury Chest", "Rp. 15500000",
            "https://cdn.shopify.com/s/files/1/2350/5189/products/Newbury_Chest_4_Drawers_ac359dbd-76f7-48c5-8c15-7d968c69e18b_720x.jpg?v=1569092316"),
        _produk("Nara L-Seat Sofa", "Rp. 12000000",
            "https://cdn.shopify.com/s/files/1/2350/5189/products/Nara_L-Shape_2_seat_720x.jpg?v=1569071784"),
        _produk("Vinoti Living", "Rp. 12000000",
            "https://cdn.shopify.com/s/files/1/2350/5189/products/Nara_3_seat_540x.jpg?v=1580272503"),
      ],
    );
  }

  Widget _grid() {
    return new Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: GridView.count(
        childAspectRatio: 8.0 / 9.0,
        shrinkWrap: true,
        mainAxisSpacing: 20,
        crossAxisCount: 2,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.asset(
                      "assets/pembayaran.png",
                      cacheHeight: 115,
                      cacheWidth: 115,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "pembayaran",
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.asset(
                      "assets/transfer.png",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Transfer",
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.asset(
                      "assets/creditcard.png",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Credit Card",
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.asset(
                      "assets/rekeningku.png",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Rekeningku",
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.asset(
                      "assets/investasi.png",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Investasi",
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.transparent,
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.asset(
                      "assets/e-shop.png",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "e-shop",
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              )),
            ),
          ),
        ],
      ),
    );
  }

  Widget _produk(String nama, String harga, String url) {
    return new Container(
        color: Warna.grey,
        child: Card(
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: Image.asset(
                  url,
                  // "https://asset-a.grid.id/crop/0x0:0x0/360x240/photo/2020/04/09/663219154.png",
                  fit: BoxFit.cover,
                  width: 100,
                  height: 100,
                ),
              ),
              Container(
                padding: EdgeInsets.all(5),
                height: 100,
                margin: EdgeInsets.only(left: 8),
                child: Column(
                  children: [
                    Text(
                      nama,
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.black87,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(20),
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.ac_unit_rounded,
                          color: Colors.lightBlueAccent,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "Snow Furniture",
                          style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Warna.biruMuda,
                              backgroundColor: Colors.transparent),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                alignment: Alignment.bottomRight,
                padding: EdgeInsets.only(left: 20, bottom: 10),
                height: 100,
                child: Text(
                  harga,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        ));
  }
}
