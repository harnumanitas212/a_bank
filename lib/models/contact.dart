class Contact {
  int _id;
  String _name;
  String _username;
  String _deskripsi;
  String _foto;

  // konstruktor versi 1
  Contact(this._name, this._username, this._deskripsi, this._foto);

  // konstruktor versi 2: konversi dari Map ke Contact
  Contact.fromMap(Map<String, dynamic> map) {
    this._id = map['id'];
    this._name = map['name'];
    this._username = map['username'];
    this._deskripsi = map['deskripsi'];
    this._foto = map['foto'];
  }
  //getter dan setter (mengambil dan mengisi data kedalam object)
  // getter
  int get id => _id;
  String get name => _name;
  String get username => _username;
  String get deskripsi => _deskripsi;
  String get foto => _foto;

  // setter
  set name(String value) {
    _name = value;
  }

  set username(String value) {
    _username = value;
  }

  set deskripsi(String value) {
    _deskripsi = value;
  }

  set foto(String value) {
    _foto = value;
  }

  // konversi dari Contact ke Map
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id'] = this._id;
    map['name'] = name;
    map['username'] = username;
    map['deskripsi'] = deskripsi;
    map['foto'] = foto;
    return map;
  }
}
